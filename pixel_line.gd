extends Node2D


func _draw():
	var p0 = get_node("Point1").position
	var p1 = get_node("Point2").position
	draw_pixel_line(p0, p1, Color.white)
	draw_line(p0 + Vector2(0, 10), p1 + Vector2(0, 10), Color.white)


func draw_pixel_line(p0: Vector2, p1: Vector2, rope_color: Color):
	var dist_vec = p1 - p0
	var pos = Vector2.ZERO
	if abs(dist_vec.x) > abs(dist_vec.y):
		for i in ceil(abs(dist_vec.x)):
			pos = p0 + i * dist_vec / abs(dist_vec.x)
			pos.x = floor(pos.x)
			pos.y = floor(pos.y)
			var rect = Rect2(pos, Vector2(1, 1))
			draw_rect(rect, rope_color)
	else:
		for i in ceil(abs(dist_vec.y)):
			pos = p0 + i * dist_vec / abs(dist_vec.y)
			pos.x = floor(pos.x)
			pos.y = floor(pos.y)
			var rect = Rect2(pos, Vector2(1, 1))
			draw_rect(rect, rope_color)
